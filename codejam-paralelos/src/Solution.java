import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Solution {

    protected static Scanner in = new Scanner(new BufferedReader(new InputStreamReader(System.in)));

    public static void main(String[] args) {

        int cases = in.nextInt();

        List<Result> results = IntStream.range(0, cases)
                .mapToObj(i -> readDataCase(i, in))
                .collect(Collectors.toList());


        results.stream()
                .parallel()
                .forEach(Solution::makeMagic);

        results
                .forEach(System.out::println);

    }

    private static Result readDataCase(int caseNumber, Scanner sc) {

        Result result = new Result();
        int numberOfDigits = sc.nextInt();


        Integer[] numbers = new Integer[numberOfDigits];
        for (int i = 0; i < numberOfDigits; i++) {
            numbers[i] = sc.nextInt();
        }

        result.setDataOfCase(numbers);
        result.setNumberOfCase(caseNumber+1);

        return result;
    }


    private static void makeMagic(Result result) {

        int numberOfDigits = result.getDataOfCase().length;
        Integer[] numbers = result.getDataOfCase();

        executeAlgorithm(numberOfDigits,numbers);

        int sorted = isSorted(numbers);
        String solution = sorted == -1 ? "OK" : String.valueOf(sorted);

        result.setResult(solution);
    }

    private static void executeAlgorithm(Integer numberOfDigits, Integer[] numbers) {
        boolean changing = true;
        while (changing) {
            changing = false;
            for (int number = 0; number < numberOfDigits - 2; number++) {
                Integer[] threePositions = getThreePositions(numbers, number);
                if (isSwapping(threePositions)) {
                    changing = true;
                    threePositions = reverse(threePositions);
                    replaceNumbers(numbers, threePositions, number);
                }
            }
        }
    }


    private static Integer[] getThreePositions(Integer[] numbersToBeSort, int index) {
        Integer[] tiplet = new Integer[3];
        tiplet[0] = numbersToBeSort[index];
        tiplet[1] = numbersToBeSort[index + 1];
        tiplet[2] = numbersToBeSort[index + 2];
        return tiplet;
    }


    private static Integer[] reverse(Integer[] numbersToBeSort) {
        Integer aux = numbersToBeSort[0];
        numbersToBeSort[0] = numbersToBeSort[2];
        numbersToBeSort[2] = aux;
        return numbersToBeSort;
    }


    private static Integer[] replaceNumbers(Integer[] numbersToBeSort, Integer[] numbersToBeReplaced, int index) {
        int digitToReplace = numbersToBeReplaced.length;
        if (digitToReplace > 0) {
            numbersToBeSort[index] = numbersToBeReplaced[0];
        }
        if (digitToReplace > 1) {
            numbersToBeSort[index + 1] = numbersToBeReplaced[1];
        }
        if (digitToReplace > 2) {
            numbersToBeSort[index + 2] = numbersToBeReplaced[2];
        }
        return numbersToBeSort;
    }
    private static int isSorted(Integer[] number) {
        for (int index = 0; index < number.length; index++) {
            if (index < number.length - 1 && number[index] > number[index+1]) {
                return index;
            }
        }
        return -1;
    }


    private static boolean isSwapping(Integer[] numbers) {
        if (numbers[0] > numbers[2]) {
            return true;
        } else {
            return false;
        }
    }

    public static class Result {

        private int numberOfCase;
        private Integer[] dataOfCase;
        private String result;

        public int getNumberOfCase() {
            return numberOfCase;
        }

        public void setNumberOfCase(int numberOfCase) {
            this.numberOfCase = numberOfCase;
        }

        public Integer[] getDataOfCase() {
            return dataOfCase;
        }

        public void setDataOfCase(Integer[] dataOfCase) {
            this.dataOfCase = dataOfCase;
        }

        public String getResult() {
            return result;
        }

        public void setResult(String result) {
            this.result = result;
        }

        @Override
        public String toString() {
            return "Case #" + getNumberOfCase() + ": " + getResult();
        }
    }


}
