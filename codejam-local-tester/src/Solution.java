import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Solution {

    protected static Scanner in = new Scanner(new BufferedReader(new InputStreamReader(System.in)));;


    public static void main(String[] args) {

        int cases = Integer.parseInt(in.nextLine());
        int numberOfCase;
        for (numberOfCase = 1; numberOfCase <= cases; numberOfCase++) {
            String[] split = in.nextLine().split(" ");
            int maxDamage = Integer.parseInt(split[0]);
            StringBuilder program = split.length == 1 ? null : new StringBuilder(split[1]);
            if (program == null) {
                if (maxDamage == 0){
                    System.out.println("Case #" + numberOfCase + ": 0");
                } else {
                    System.out.println("Case #" + numberOfCase + ": IMPOSSIBLE");
                }
            }

            int hackingMoves = 0;
            while (true) {
                int damage = calcucateDamage(program);
                if (maxDamage >= damage) {
                    System.out.println("Case #" + numberOfCase + ": " + hackingMoves);
                    break;
                } else {
                    boolean instructionChanged = swapLastChargeShoot(program);
                    if (!instructionChanged) {
                        System.out.println("Case #" + numberOfCase + ": IMPOSSIBLE");
                        break;
                    }
                }
                hackingMoves++;
            }
        }
    }

    private static int calcucateDamage(StringBuilder program) {
        if (program == null || program.length() == 0) {
            return 0;
        } else {
            int powerCharged = 1;
            int totalDamage = 0;
            for (int instruction = 0; instruction < program.length(); instruction++) {
                char instructionAnalized = program.charAt(instruction);
                if ('S' == instructionAnalized) {
                    totalDamage += powerCharged;
                } else if ('C' == instructionAnalized) {
                    powerCharged = powerCharged * 2;
                }
            }
            return totalDamage;
        }
    }

    public static boolean swapLastChargeShoot(StringBuilder program) {
        int lastIndex = program.lastIndexOf("CS");
        if (lastIndex != -1) {
            program.setCharAt(lastIndex, 'S');
            program.setCharAt(lastIndex + 1, 'C');
            return true;
        } else {
            return false;
        }
    }


}